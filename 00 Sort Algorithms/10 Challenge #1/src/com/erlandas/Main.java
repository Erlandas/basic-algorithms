package com.erlandas;

/***
 * Challenge #1:
 *      1.  Modify the merge sort algorithm so that it sorts integers in
 *          DESCENDING order
 *      2.  Use the usual example array
 *      3.  You can start with the project in the resources section
 */

public class Main {

    public static void main(String[] args) {

        //Array we working on
        int[] array = {20, 35, -15, 7, 55, 1, -22};


        mergeSortNoComments(array,0,array.length);

        //Code for printing out an array
        for (int i : array) {
            System.out.print(i + ", ");
        }
    }

    //Merge sort implementation recursive method
    public static void mergeSort(int[] inputArray, int startIndex, int endIndex) {

        //--    breaking condition for recursion
        //--    if there is only 1 element in the array
        if (endIndex - startIndex < 2) {
            return;
        }

        //--    initialize middle point of the array
        int middleIndex = (startIndex + endIndex) / 2;

        //--    RECURSIVE call for the LEFT side array
        //--    inputArray -> it's always be the same input array
        //--    startIndex -> left array starts from the index zero
        //--    middleIndex -> end index for the left array will be middle
        mergeSort(inputArray,startIndex,middleIndex);

        //--    RECURSIVE call for the RIGHT side array
        //--    inputArray -> it's always be the same input array
        //--    middleIndex -> right array starts from the middle
        //--    endIndex -> end index for the right array will be last element of the actual array.length
        mergeSort(inputArray,middleIndex,endIndex);

        //--    inputArray -> takes in an actual array
        //--    startIndex -> where does partition starts
        //--    middleIndex -> where is the middle
        //--    endIndex -> where does partition ends
        merge(inputArray,startIndex,middleIndex,endIndex);

    }

    //Method for merging++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static void merge(int[] inputArray, int startIndex, int middleIndex, int endIndex) {

        //--    Modification
        //--    if that is true we done we don't actually have to do any sorting
        //--    if(last element in the left partition <= first element in the right partition)
        //--    if that's true that means all the elements in the left partition are less
        //--    than or equal than the smallest element in the right partition
        //--    because we are merging SORTED partitions
        //--    This optimization is stopping us from going ahead and merging our left and right array
        //--    when we don't have to do that because the result of the merge is going to equal
        //--    what we already have in the inputArray.
        //--    And this works because we know at this point that both the left and right partitions are sorted.
        //--    If they weren't sorted, we could not do this check.
        if(inputArray[middleIndex - 1] <= inputArray[middleIndex]) {
            return;
        }

        int start = startIndex;
        int middle = middleIndex;
        int tempIndex = 0;

        int[] tempArray = new int[endIndex - startIndex];

        //--    If one of arrays reached its end index we drop out of the loop
        while(start < middleIndex && middle < endIndex) {
            //--    we are going to compare the current element in the left partition
            //--    with the current element in the right partition (we keep a track of elements "start" and "middle" variables)
            //--    and we going to to write the smaller of the two to the tempArray.
            //--    Now because the merge sort is stable we using "<=" in comparison.
            //--    If we wont have "=" there the merge sort will become unstable algorithm.
            //--    When we assigning element in the tempArray we also increment its index in the [tempIndex++]
            //--    Also we incrementing index of "start" or "middle" depending on the one we writing into tempArray.
            tempArray[tempIndex++] = inputArray[start] <= inputArray[middle] ? inputArray[start++] : inputArray[middle++];
        }

        //--    OPTIMIZATION
        //--    This is going to be handling the remaining elements
        //--    in the array that we have in traverse.
        //--    If we have elements left in the left array we copy those elements in the tempArray
        //--    if we have elements left in the right array wo do NOTHING as all the remaining elements
        //--    already exists in the correct positions in the inputArray
        //--    Remember we copying sorted arrays so that means remainder of the right array
        //--    is in sorted order so if we will be copying we will be overwriting same elements at the same positions

        //--    System.arraycopy(source, source position, destination, destination position, length)
        //--    source -> from where to copy
        //--    start -> from where start copying
        //--    destination -> where to copy
        //--    destination position -> where to start put new elements copied
        //--    length -> how many elements to copy
        System.arraycopy(inputArray, start, inputArray, startIndex + tempIndex, middleIndex - start);

        //--    Now copy contents of temp array into inputArray
        System.arraycopy(tempArray, 0, inputArray, startIndex, tempIndex);

    }
//==========================================================================================================================
    //Merge sort no COMMENTS
    public static void mergeSortNoComments(int[] inputArray, int startIndex, int endIndex) {
        if (endIndex - startIndex < 2) {
            return;
        }
        int middleIndex = (startIndex + endIndex) / 2;
        mergeSortNoComments(inputArray,startIndex,middleIndex);
        mergeSortNoComments(inputArray,middleIndex,endIndex);
        mergeNoComments(inputArray,startIndex,middleIndex,endIndex);
    }

    //Method for merging no COMMENTS
    public static void mergeNoComments(int[] inputArray, int startIndex, int middleIndex, int endIndex) {
        //We don't have to remove this statement
        //enough to change from <=, to >=
//        if(inputArray[middleIndex - 1] <= inputArray[middleIndex]) {
//            return;
//        }

        int start = startIndex;
        int middle = middleIndex;
        int tempIndex = 0;
        int[] tempArray = new int[endIndex - startIndex];

        while(start < middleIndex && middle < endIndex) {
            //Changes made:                     from <= , to >=
            tempArray[tempIndex++] = inputArray[start] >= inputArray[middle] ? inputArray[start++] : inputArray[middle++];
        }

        System.arraycopy(inputArray, start, inputArray, startIndex + tempIndex, middleIndex - start);
        System.arraycopy(tempArray, 0, inputArray, startIndex, tempIndex);

    }
}
