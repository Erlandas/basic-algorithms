package com.erlandas;

public class Main {

    public static void main(String[] args) {

        System.out.println(recursiveFactorial(6));
        System.out.println(iterativeFactorial(6));

    }
    //--    1! = 1 * 0! = 1
    //--    2! = 2 * 1 = 2 * 1!
    //--    3! = 3 * 2 * 1 = 3 * 2!
    //--    4! 4 * 3 * 2 * 1 = 4 * 3!

    //--    n! = n * (n - 1)!

    //Recursive method to calculate the factorial
    public static int recursiveFactorial(int num) {

        //--    We always need a condition to break recursive call
        //--    And that condition is known as the breaking condition
        //--    And is also called the base case

        if(num == 0) return 1;// if we don't have this condition recursive call wont end, The base case.

        return num * recursiveFactorial(num-1);
    }

    //Method that counts factorial no recursion iterative solution used
    public static int iterativeFactorial(int num) {

        //--    First we check if number passed equals to 0
        if(num == 0) {
            return 1;
        }

        //--    If num doesn't equal to 0
        int factorial = 1;
        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }

        return factorial;
    }

}
