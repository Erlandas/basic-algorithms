package com.erlandas;

public class Main {

    public static void main(String[] args) {

        //Array we working on
        int[] array = { 2, 5, 9, 8, 2, 8, 7, 10, 4, 3 };

        countingSort(array,1,10);

        //Printing the array
        for (int i : array) {
            System.out.print(i + ", ");
        }
    }

    //Counting sort method
    //--    Actual array we working on
    //--    Minimum value in the range
    //--    Maximum value in the range
    public static void countingSort(int[] inputArray, int minValue, int maxValue) {

        //--    create array that's going to track our counting
        //--    this array has to be large enough to count each possible value
        int[] countArray = new int[(maxValue-minValue)+1];

        for(int i = 0 ; i < inputArray.length; i++) {
            countArray[inputArray[i]-minValue]++;
        }

        //--    j is the index we are going to use to write to the input array
        //--    its keep tracking where we at at the array
        int j = 0;

        //--    i is the index that we are going to use to traverse the countArray
        for(int i = minValue; i <= maxValue; i++) {

            //-- loop runs until values to write is greater than 0
            while(countArray[i - minValue] > 0) {
                //-- we writing to the array current i value
                inputArray[j++] = i;

                //-- we decrementing amount of current values
                countArray[i - minValue]--;
            }

        }


    }
}
