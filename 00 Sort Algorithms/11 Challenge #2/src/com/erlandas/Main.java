package com.erlandas;

/***
 * Challenge #3:
 *      1.  Change insertion sort so that it uses recursion
 *      2.  Sort the usual example array
 *      3.  You can start with the project in the resources section
 */

public class Main {

    public static void main(String[] args) {

        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        insertionSort(intArray,intArray.length);

//        for(int firstUnsortedIndex = 1; firstUnsortedIndex < intArray.length ; firstUnsortedIndex++) {
//            int newElement = intArray[firstUnsortedIndex];
//            int i;
//
//            for (i = firstUnsortedIndex; i > 0 && intArray[i-1] > newElement; i--) {
//                intArray[i] = intArray[i-1]; //Shifting
//            }
//            intArray[i] = newElement;
//        }

        for (int i : intArray) {
            System.out.print(i + ", ");
        }
    }

    public static void insertionSort(int[] intArray, int numItems) {
        if(numItems < 2) {
            return;
        }

        insertionSort(intArray,numItems-1);

        int newElement = intArray[numItems-1];
        int i;

        for (i = numItems-1; i > 0 && intArray[i-1] > newElement; i--) {
            intArray[i] = intArray[i-1]; //Shifting
        }
        intArray[i] = newElement;

        System.out.println("Result of call when numItems = " + numItems);

        for (int i1 : intArray) {
            System.out.print(i1 + ", ");
        }
        System.out.println();
        System.out.println("-----------------------------------");
    }

}
