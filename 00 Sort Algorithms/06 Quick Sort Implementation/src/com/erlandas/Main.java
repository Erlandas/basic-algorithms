package com.erlandas;

public class Main {

    public static void main(String[] args) {
        //Array we working on
        int[] array = {20, 35, -15, 7, 55, 1, -22};

        quickSortNoComments(array,0,array.length);

        //Code for printing out an array
        for (int i : array) {
            System.out.print(i + ", ");
        }
    }

    //Quick sort method
    public static void quickSort(int[] inputArray, int startIndex, int endIndex) {
        //--    Prevent dealing with 1 element array
        if(endIndex - startIndex < 2) {
            return;
        }

        //--    this method will return is the position of the element in the sorted array
        //--    so its correct sorted position
        int pivotIndex = partition(inputArray, startIndex, endIndex);
        quickSort(inputArray, startIndex, pivotIndex);  //--    quickSort left sub-array
        quickSort(inputArray, pivotIndex+1, endIndex);  //--    quickSort right sub-array

    }

    //Partition method
    public static int partition(int[] inputArray, int startIndex, int endIndex) {
        //NOTE: this is using the first element as the pivot
        int pivot = inputArray[startIndex];

        int start = startIndex;
        int end = endIndex;

        while(start < end) {

            //NOTE: empty loop body
            //--    So we are just basically using the loop to keep decrementing "end" or "j" in notes variable
            //--    until we either find an element that's less than the pivot or start crosses end
            while(start < end && inputArray[--end] >= pivot);

            //--    first we have to make sure start doesn't crosses end
            //--    if condition met then we want to move the element at index "end" to index "start"
            //--    because basically we have found the first element that is less than the pivot
            //--    and so we want to move that towards the front of the array
            if(start < end) {
                inputArray[start] = inputArray[end];
            }

            //--    we using a loop to increment "start" (i in notes) value until we find an element
            //--    that's greater than the pivot or equal
            while(start < end && inputArray[++start] <= pivot);

            //--    first we have to make sure start doesn't crosses end
            //--    if condition met then we want to move the element at index "start" to index "end"
            //--    because basically we have found the first element that is greater than the pivot
            //--    and so we want to move that towards the end of the array
            if(start < end) {
                inputArray[end] = inputArray[start];
            }

        }

        //--    At this point when we drop out of the while loop the value of "end" variable
        //--    will be the index where we want to insert the pivot and that's all we have left to do
        inputArray[end] = pivot;

        //--    We returning value of "end" (j in notes) because that's how we divide the array
        //--    and we need that to then call quick sort for the left array and then for the right array
        return end;

    }

    //=================NO COMMENTS============================
    //Quick sort method
    public static void quickSortNoComments(int[] inputArray, int startIndex, int endIndex) {
        if(endIndex - startIndex < 2) {
            return;
        }
        int pivotIndex = partitionNoComments(inputArray, startIndex, endIndex);
        quickSort(inputArray, startIndex, pivotIndex);
        quickSort(inputArray, pivotIndex+1, endIndex);
    }

    //Partition method
    public static int partitionNoComments(int[] inputArray, int startIndex, int endIndex) {
        //NOTE: this is using the first element as the pivot
        int pivot = inputArray[startIndex];

        int start = startIndex;
        int end = endIndex;

        while(start < end) {

            while(start < end && inputArray[--end] >= pivot);
            if(start < end) {
                inputArray[start] = inputArray[end];
            }

            while(start < end && inputArray[++start] <= pivot);
            if(start < end) {
                inputArray[end] = inputArray[start];
            }
        }
        inputArray[end] = pivot;
        return end;
    }
}
