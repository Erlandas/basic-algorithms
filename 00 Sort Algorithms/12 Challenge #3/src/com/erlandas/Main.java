package com.erlandas;

//======================================================
//RADIX SORT TO SORT A STRING
public class Main {

    public static void main(String[] args) {

        //Array we working on
        String[] radixArray = { "bcdef","dbaqc","abcde","omadd","bbbbb" };

        radixSortEx(radixArray,26,5);


        //Code to print out the radixArray
        for (String i : radixArray) {
            System.out.print(i + ", ");
        }
    }


    public static void radixSortEx(String[] input, int radix, int width) {
        //loop to take last character index starts backwards
        for(int i = width-1; i >= 0; i--){
            radixSingleSortEx(input, i , radix);
        }
    }

    private static void radixSingleSortEx(String[] input, int position, int radix) {

        int numItems = input.length;
        int[] countArray = new int[radix];

        //--    For every value in our input array
        for(String value: input) {
            //--    this method going to return digit 0 - 25
            //--    array incremented by 1 in returned integers position
            countArray[getIndex(position,value)]++;
        }

        //--    Sum up all the counts and including the digit that we are working on
        //--    Adjust the count array
        for(int j = 1; j < radix; j++) {
            countArray[j] += countArray[j-1];
        }

        //--    We are going to copy the values into a temporary array,
        //--    we are going to work from right to left so that we are preserving
        //--    the relative ordering of duplicates
        String[] temp = new String[numItems];

        for (int tempIndex = numItems-1; tempIndex >= 0; tempIndex--) {
            temp[--countArray[getIndex(position, input[tempIndex])]] = input[tempIndex];//??
        }

        //Now copy the values from temp[] in to input[]
        for(int tempIndex = 0; tempIndex < numItems; tempIndex++) {
            input[tempIndex] = temp[tempIndex];
        }


    }

    public static int getIndex(int position, String value) {
        //Translate letters 0 - 25, -a if lets say b = 98 - 97(a) = b position is 1..
        //Working with ASCII values
        return value.charAt(position) - 'a';
    }
}
