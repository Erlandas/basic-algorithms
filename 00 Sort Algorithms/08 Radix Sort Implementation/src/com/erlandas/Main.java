package com.erlandas;

public class Main {

    public static void main(String[] args) {

        //Array we working on
        int[] radixArray = { 4725, 4586, 1330, 8792, 1594, 5729 };

        radixSort(radixArray,10,4);


        //Code to print out the radixArray
        for (int i : radixArray) {
            System.out.print(i + ", ");
        }
    }

    public static void radixSort(int[] input, int radix, int width) {
        for(int i = 0; i < width; i++){
            radixSingleSort(input, i , radix);
        }
    }

    private static void radixSingleSort(int[] input, int position, int radix) {

        int numItems = input.length;
        int[] countArray = new int[radix];

        //--    For every value in our input array
        for(int value: input) {
            //--    this method going to return digit 0 - 9
            //--    array incremented by 1 in returned integers position
            countArray[getDigit(position,value,radix)]++;
        }

        //--    Sum up all the counts and including the digit that we are working on
        //--    Adjust the count array
        for(int j = 1; j < radix; j++) {
            countArray[j] += countArray[j-1];
        }

        //--    We are going to copy the values into a temporary array,
        //--    we are going to work from right to left so that we are preserving
        //--    the relative ordering of duplicates
        int[] temp = new int[numItems];

        for (int tempIndex = numItems-1; tempIndex >= 0; tempIndex--) {
            temp[--countArray[getDigit(position, input[tempIndex],radix)]] = input[tempIndex];//??
        }

        //Now copy the values from temp[] in to input[]
        for(int tempIndex = 0; tempIndex < numItems; tempIndex++) {
            input[tempIndex] = temp[tempIndex];
        }


    }

    public static int getDigit(int position, int value, int radix) {
        //In first go 1's position
        //as value we pass 4725, position will be 0 (everything to the power of 0 is 1) and the radix 10 (digits 0 - 9)
        //division operation has precedents before modulus operator
        //So, 4725 / 10^0 % 10 => 4725 / 1 => 4725 % 10 => 5
        //--------------------
        //10's position
        //4725 / 10^1 % 10 => 4725 / 10 => 472 % 10 => 2
        //100's position
        //4725 / 10^2 % 10 => 4725 / 100 => 47 % 10 => 7
        //1000's position
        //4725 / 10^3 % 10 => 4725 / 1000 => 4 % 10 => 4
        return value / (int) Math.pow(radix, position) % radix;
    }
}
