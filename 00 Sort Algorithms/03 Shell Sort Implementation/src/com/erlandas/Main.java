package com.erlandas;

public class Main {

    public static void main(String[] args) {

        //Array we working on
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        //--    int gap = intArray.length / 2  -> because we are going to start with gap 3
        //--    gap > 0 -> we are going to run until gap is greater than zero because if gap is 0 we will be comparing elements against themselves
        //--    gap /= 2 -> on each iteration we are going to divide gap value by 2
        for(int gap = intArray.length / 2 ; gap > 0 ; gap /= 2) {

            //--    Down there is actually comparing and shifting
            //--    i < intArray.length -> because we want to look to all the elements in the array followed from i
            for(int i = gap; i < intArray.length; i++) {

                int newElement = intArray[i];   //  assign newElement field with current i position is the value what we are going to be looking at
                                                //  that's essentially intArray[gap]

                int j = i;  //  we will be using j to do traversing

                //--    j >= gap -> because if j becomes less than a gap, that means that we have hit the front of the array
                //--    intArray[j - gap] > newElement -> looking for larger element
                while(j >= gap && intArray[j - gap] > newElement) {

                    //--    If condition met we want to shift the element at inArray[j-gap] up gap position
                    intArray[j] = intArray[j - gap];

                    //--    And once we done that we want to subtract gap from j
                    //--    Because we are saying that now we are going to want to compare
                    //--    newElement with whatever comes three positions over
                    j -= gap;
                }

                //--   after we drop out of the loop we basically saying we found the insertion point
                //--   and so we are going to assign intArray[j] with newElement
                intArray[j] = newElement;

            }

        }

        //Code for printing out an array
        for (int i : intArray) {
            System.out.print(i + ", ");
        }
    }
}
