package com.erlandas;

public class EmployeeDoublyLinkedList {

    private EmployeeNode head;
    private EmployeeNode tail;
    private int size;

    public void addToFront(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);
        node.setNext(head);

        //If we adding to empty list
        if (head == null) {
            tail = node;
        } else {
            head.setPrevious(node);
        }

        head = node;
        size++;
    }

    public void addToEnd(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);

        if(tail == null) {
            head = node;// set the head to the new node
        } else {

            //--    example null<>john<>null
            //--    we adding jane
            //--    current tail is john we set to point to jane
            //--    then we set previuos element for jane to point to current tail
            //--    we get null <=> john <=> jane <=> null
            tail.setNext(node);//current tails next field to the node we adding
            node.setPrevious(tail);// previous field of the node we are adding
        }

        tail = node; //final step set the tail to node
        size++;

    }

    public EmployeeNode removeFromFront() {
        if(isEmpty()) {
            return null;
        }

        EmployeeNode removedNode = head;

        if(head.getNext() == null) {
            tail = null;
        } else {
            head.getNext().setPrevious(null);
        }


        head = head.getNext();
        size--;
        removedNode.setNext(null);
        return removedNode;
    }

    public EmployeeNode removeFromEnd() {
        if(isEmpty()) {
            return null;
        }

        EmployeeNode removedNode = tail;

        if(tail.getPrevious() == null) { // if its 1 item in a list
            head = null;
        } else {
            tail.getPrevious().setNext(null);
        }

        tail = tail.getPrevious();
        size--;
        removedNode.setPrevious(null);
        return removedNode;

    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public void printList() {
        EmployeeNode current = head;
        System.out.print("HEAD -> ");
        while(current != null) {
            System.out.print(current);
            System.out.print(" <=> ");
            current = current.getNext();
        }
        System.out.print("null");
        System.out.println();
    }


}
