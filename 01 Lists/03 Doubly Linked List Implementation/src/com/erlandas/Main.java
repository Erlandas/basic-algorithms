package com.erlandas;

public class Main {

    public static void main(String[] args) {

        Employee janeJones = new Employee("Jane","Jones",123);
        Employee johnDoe = new Employee("John","Doe",4567);
        Employee marrySmith = new Employee("Marry","Smith",22);
        Employee mikeWilson = new Employee("Mike","Wilson",3245);

        EmployeeDoublyLinkedList list = new EmployeeDoublyLinkedList();
        list.addToFront(janeJones);
        list.addToFront(johnDoe);
        list.addToFront(marrySmith);
        list.addToFront(mikeWilson);



        list.printList();
        Employee billEnd = new Employee("Bill","End",78);
        list.addToEnd(billEnd);
        list.printList();

        list.removeFromFront();
        list.printList();

        list.removeFromEnd();
        list.printList();
    }
}
