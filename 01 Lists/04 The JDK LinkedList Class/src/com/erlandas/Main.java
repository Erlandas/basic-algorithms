package com.erlandas;

import org.w3c.dom.ls.LSOutput;

import java.util.Iterator;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {

        Employee janeJones = new Employee("Jane","Jones",123);
        Employee johnDoe = new Employee("John","Doe",4567);
        Employee marrySmith = new Employee("Marry","Smith",22);
        Employee mikeWilson = new Employee("Mike","Wilson",3245);

        Employee billEnd = new Employee("Bill", "End",78);

        //--    Create LinkedList for Employee class
        LinkedList<Employee> list = new LinkedList<>();

        //--    Add at the beginning of the list
        list.addFirst(janeJones);
        list.addFirst(johnDoe);
        list.addFirst(marrySmith);
        list.addFirst(mikeWilson);

        printList(list);

        //--    OR (even using standard for loop will do the job)
        //list.forEach(employee -> System.out.println(employee));


        //--    Add method adds to the end
        list.add(billEnd);
        printList(list);

        //--    Remove method for last element we can removeFirst
        list.removeFirst();
        printList(list);
    }

    public static void printList(LinkedList<Employee> list) {
        //--    Print whats in the list
        Iterator iter = list.iterator();
        System.out.print("HEAD -> ");
        while(iter.hasNext()){
            System.out.print(iter.next());
            System.out.print("<=>");
        }
        System.out.println("null\n");
    }
}
