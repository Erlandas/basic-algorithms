package com.erlandas;

/***
 * Challenge #5
 *      1.  Implement the addBefore() method for the EmployeeDublyLinkedList class
 *      2.  Use the starter project in the resources section
 *      3.  Project contains all the code you need and an empty addBefore method
 *      Main method contains code that should work when you are finished
 */

public class Main {

    public static void main(String[] args) {

        Employee janeJones = new Employee("Jane","Jones",123);
        Employee johnDoe = new Employee("John","Doe",4567);
        Employee marrySmith = new Employee("Marry","Smith",22);
        Employee mikeWilson = new Employee("Mike","Wilson",3245);
        Employee billEnd = new Employee("Bill", "End", 78);

        EmployeeDoublyLinkedList list = new EmployeeDoublyLinkedList();
        list.addToFront(janeJones);
        list.addToFront(johnDoe);
        list.addToFront(marrySmith);
        list.addToFront(mikeWilson);

        list.addBefore(billEnd, johnDoe);
        list.addBefore(new Employee("Someone", "Else", 1111), mikeWilson);
        list.printList();
    }
}
