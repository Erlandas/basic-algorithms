package com.erlandas;
/***
 *      Vector is the same as ArrayList but its synchronized
 *      if we need to have synchronized code we will use Vector
 *      if not its recommended to use ArrayList to don't have an
 *      overhead with synchronization
 *
 *      Vector is thread safe
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Main {

    public static void main(String[] args) {

        List<Employee> employeeList = new Vector<>();
        employeeList.add(new Employee("Jane","Jones",123));
        employeeList.add(new Employee("John","Doe",4567));
        employeeList.add(new Employee("Marry","Smith",22));
        employeeList.add(new Employee("Mike","Wilson",3245));
    }
}
