package com.erlandas;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("Jane","Jones",123));
        employeeList.add(new Employee("John","Doe",4567));
        employeeList.add(new Employee("Marry","Smith",22));
        employeeList.add(new Employee("Mike","Wilson",3245));

//        divide("print using lambdas");
//        employeeList.forEach(employee -> System.out.println(employee));
//
//        divide("Get item zero based");
//        System.out.println(employeeList.get(1));
//
//        divide("isEmpty");
//        System.out.println(employeeList.isEmpty());

        divide("Replace object set method");
        System.out.println("employeeList.set(1,new Employee(\"John\",\"Adams\", 4568));");
        employeeList.set(1,new Employee("John","Adams", 4568));
        divide("print");
        employeeList.forEach(employee -> System.out.println(employee));
        divide("get number of items (size)");
        System.out.println(employeeList.size());

        divide("list to array");
        Employee[] employeeArray = employeeList.toArray(new Employee[employeeList.size()]);
        for (Employee employee : employeeArray) {
            System.out.println(employee);
        }

        divide("Contains");
        System.out.println(employeeList.contains(new Employee("Marry","Smith",22)));

        divide("get index of an object");
        System.out.println(employeeList.indexOf(new Employee("Marry","Smith",22)));

        divide("remove items");
        employeeList.remove(1);
        employeeList.forEach(employee -> System.out.println(employee));


    }

    public static void divide(String msg) {
        System.out.println("\n==================== "+msg+" =====================");
    }
    public static void divide() {
        System.out.println("\n=========================================");
    }
}
