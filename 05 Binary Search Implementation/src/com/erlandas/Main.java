package com.erlandas;

public class Main {

    public static void main(String[] args) {

        int[] array = { -22, -15, 1 , 7 , 20, 35, 55};

//        System.out.println(iterativeBinarySearch(array, -15));
//        System.out.println(iterativeBinarySearch(array, 35));
//        System.out.println(iterativeBinarySearch(array, 8888));
//        System.out.println(iterativeBinarySearch(array, 1));

        System.out.println(recursiveBinarySearch(array, -15));
        System.out.println(recursiveBinarySearch(array, 35));
        System.out.println(recursiveBinarySearch(array, 8888));
        System.out.println(recursiveBinarySearch(array, 1));

    }

    //Iterative method for binary search
    public static int iterativeBinarySearch(int[] input, int value) {
        int start = 0;
        int end = input.length;

        //--    Because when start equals end we have searched
        //--    the entire array and we have not found what we are looking for
        while(start < end) {
            int midpoint = (start + end) / 2;
            System.out.println("Midpoint = " + midpoint);
            if(input[midpoint] == value) {
                return midpoint;
            }
            //--    If true searching right side of the array
            else if (input[midpoint] < value) {
                start = midpoint + 1;
            }
            //--    If true searching left side of the array
            else {
                end = midpoint;
            }
        }
        return -1;

    }

    //Recursive method for binary search
    public static int recursiveBinarySearch(int[] input, int value) {
        return recursiveBinarySearch(input, 0, input.length, value);
    }

    public static int recursiveBinarySearch(int[] input, int start, int end, int value) {
        //--    Breaking condition
        if(start >= end) {
            return -1;
        }

        int midpoint = (start + end) / 2;
        System.out.println("Midpoint = " + midpoint);

        if(input[midpoint] == value) {
            return midpoint;
        }
        else if(input[midpoint] < value) {
            return recursiveBinarySearch(input,midpoint+1,end,value);
        }
        else {
            return recursiveBinarySearch(input,start,midpoint,value);
        }
    }
}
