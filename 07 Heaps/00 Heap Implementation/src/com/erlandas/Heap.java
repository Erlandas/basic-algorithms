package com.erlandas;

public class Heap {
    //Two field needed
    //--    1.  The array
    //--    2.  Next available position
    private int[] heap;
    private int size;

    public Heap(int capacity) {
        heap = new int[capacity];
    }

    //===========================INSERT======================================
    //Insert method
    public void insert(int value) {
        if(isFull()) {
            throw new IndexOutOfBoundsException("Heap is full");
        }

        //put new item at first available spot
        heap[size] = value;
        fixHeapAbove(size);
        size++;
    }

    private void fixHeapAbove(int index) {
        int newValue = heap[index];
        while(index > 0 && newValue > heap[getParent(index)]) {
            heap[index] = heap[getParent(index)];
            index = getParent(index);
        }

        heap[index] = newValue;
    }
    //========================DELETE HEAPS=====================================================
    //--    Two methods needed
    //--    Delete heap method
    //--    fix heap below method

    public int delete(int index) {
        if(isEmpty()) {
            throw new IndexOutOfBoundsException("Heap is empty");
        }

        int parent = getParent(index);
        int deletedValue = heap[index];

        //replacement step
        heap[index] = heap[size - 1];

        //If the value of heap[index] is greater than its parent we have to
        //fix heap above, if the value at heap[index] is less than its parent,
        //then we need to look at the heap below
        if(index == 0 || heap[index] < heap[parent]) {
            fixHeapBelow(index, size-1);
        }
        else {
            fixHeapAbove(index);
        }

        size--;
        return deletedValue;
    }

    //index of start position, index of item that we deleted
    private void fixHeapBelow(int index, int lastHeapIndex) {
        int childToSwap;

        while(index <= lastHeapIndex) {
            int leftChild = getChild(index,true);
            int rightChild = getChild(index, false);

            if(leftChild <= lastHeapIndex) {
                if (rightChild > lastHeapIndex) {
                    childToSwap = rightChild;
                } else {
                    childToSwap = (heap[leftChild] > heap[rightChild] ? leftChild : rightChild);
                }

                //We only need to swap if the value at index is less than the child
                if (heap[index] < heap[childToSwap]) {
                    int tmp = heap[index];
                    heap[index] = heap[childToSwap];
                    heap[childToSwap] = tmp;
                }
                else {
                    break;
                }
                index = childToSwap;
            }
            else {
                break;
            }
        }
    }

    //========================COMPLEMENTARY METHODS=================================================

    public int peek() {
        if(isEmpty()) {
            throw new IndexOutOfBoundsException("Heap is empty");
        }
        //peek returns only root
        return heap[0];
    }

    public void printHeap() {
        for(int i = 0 ; i < size ; i++) {
            System.out.print(heap[i] + ", ");
        }
        System.out.println();
    }

    public boolean isEmpty() {
        return size == 0;
    }

    //index of parent what child we want to get
    //left if its true we want left child if its false right child
    public int getChild(int index, boolean left) {
        return 2 * index + (left ? 1 : 2);
    }

    public boolean isFull() {
        return size == heap.length;
    }

    //index of an element in the array that we want to get parent for
    public int getParent(int index) {
        return (index - 1) / 2;
    }

    //++++++++++++++++++HEAP SORT IMPLEMENTATION+++++++++++++++++++++++++++++++++++++

    public void sort() {
        int lastHeapIndex = size -1;
        for(int i = 0 ; i < lastHeapIndex ; i++) {
            int tmp = heap[0];
            heap[0] = heap[lastHeapIndex - i];
            heap[lastHeapIndex - i] = tmp;

            fixHeapBelow(0, lastHeapIndex - i - 1);
        }
    }
}
