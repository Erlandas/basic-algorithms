package com.erlandas;
//In this implementation we not allow duplicate values
public class Tree {

    //We only need one field in the tree and that's the root node
    //Because if we have the root we can get to everything else.
    //If we have the root node we can get to everything else by
    //Traversing it's left and right child's.
    private TreeNode root;

    public void insert(int value) {
        if(root == null) {
            root = new TreeNode(value);
        } else {
            root.insert(value);
        }
    }

    public void traverseInOrder() {
        if(root != null) {
            root.traverseInOrder();
        }
    }
}
