package com.erlandas;

public class TreeNode {

    //Three fields
    //--    1.  Field for the data
    //--    2.  Right child
    //--    3.  Left child

    private int data;
    private TreeNode leftChild;
    private TreeNode rightChild;

    //--    Insert method
    public void insert(int value) {
        //--    Dealing with duplicate values
        if(value == data) {
            return;
        }

        if(value < data) {
            if(leftChild == null) {
                leftChild = new TreeNode(value);
            }
            else {
                leftChild.insert(value);
            }
        }
        else {
            if(rightChild == null) {
                rightChild = new TreeNode(value);
            }
            else {
                rightChild.insert(value);
            }
        }
    }

    //--    In-Order Traversal method
    public void traverseInOrder() {
        if(leftChild != null) {
            leftChild.traverseInOrder();
        }
        System.out.print(data+ ", ");
        if(rightChild != null) {
            rightChild.traverseInOrder();
        }
    }

    //--    When we construct the node it will be leaf node first
    //--    And so we don't have anything to assign to the left and right childs.

    public TreeNode(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public TreeNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(TreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public TreeNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(TreeNode rightChild) {
        this.rightChild = rightChild;
    }
}
