package com.erlandas;

import java.util.LinkedList;

/***
 * Stack Challenge:
 *      1.  Using a stack, determine whether a string is a palindrome
 *      2.  Strings may contain punctuation and spaces. they should be ignored.
 *          Case should be ignored
 */

public class Main {

    public static void main(String[] args) {
        // should return true
        System.out.println(checkForPalindrome("abccba"));
        // should return true
        System.out.println(checkForPalindrome("Was it a car or a cat I saw?"));
        // should return true
        System.out.println(checkForPalindrome("I did, did I?"));
        // should return false
        System.out.println(checkForPalindrome("hello"));
        // should return true
        System.out.println(checkForPalindrome("Don't nod"));
    }

    public static boolean checkForPalindrome(String string) {

        LinkedList<Character> stack = new LinkedList<>();

        StringBuilder stringNoPunctuation = new StringBuilder(string.length());
        String lowerCase = string.toLowerCase();
        for(int i = 0; i < lowerCase.length(); i++) {
            if(lowerCase.charAt(i) >= 'a' && lowerCase.charAt(i) <= 'z') {
                stringNoPunctuation.append(lowerCase.charAt(i));
                stack.push(lowerCase.charAt(i));
            }
        }

        StringBuilder reversedString = new StringBuilder(stack.size());
        while(!stack.isEmpty()) {
            reversedString.append(stack.pop());
        }

        return reversedString.toString().equals(stringNoPunctuation.toString());
    }
}
