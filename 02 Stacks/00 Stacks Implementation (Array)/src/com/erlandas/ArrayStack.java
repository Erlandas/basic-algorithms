package com.erlandas;
//NOTE: if we use [--top] it decrements actual value
//      if we using [top-1] actual top value stays the same

import java.util.EmptyStackException;

public class ArrayStack {

    //--    We need array to back the stack
    //--    we need index to track TOP position of the stack
    private Employee[] stack;
    private int top;

    public ArrayStack(int capacity) {
        stack = new Employee[capacity];
    }

    //--    Adds new employee to the top of item stack
    public void push(Employee employee) {
        //--    If evaluates to true that means stack is full
        //--    The top always would be the index were we push the next element
        //--    if the top equals stack.length that means that the next available position is equal
        //--    to the length of the array and that's actually out of bounds
        //--    so if that's true we know array is full
        //--    This operation is O(n)
        if(top == stack.length) {
            //need to resize the backing array
            //--    Just double the size of the array instead increasing by 1
            //--    that we don't need to resize after we add each item
            Employee[] newArray = new Employee[2 * stack.length];

            //--    Copy all the elements that currently exists in a stack to a newArray
            System.arraycopy(stack,0,newArray,0,stack.length);

            //--    We assign new array to the stack field
            stack = newArray;
        }
        //--    Finally we add item in to the stack at top's current position
        //--    afterwards we increment top by 1
        //--    This operation O(1)
        stack[top++] = employee;
    }

    //POP implementation removes the top position so last added item
    public Employee pop() {
        //--    First check if the stack is empty
        if(isEmpty()) {
            throw new EmptyStackException();
        }

        //--    then assign element at the current top-1 position
        Employee employee = stack[--top];
        //--    set top's element to null
        stack[top] = null;
        //--    return removed item
        return employee;

    }

    //PEEK returns item in the top's position from the stack
    public Employee peek() {
        //--    First check if the stack is empty
        if(isEmpty()) {
            throw new EmptyStackException();
        }

        return stack[top -1];

    }

    //SIZE returns size of the stack (amount of elements its currently holding)
    public int size() {
        return top;
    }

    //Checks if the array(stack) is empty
    public boolean isEmpty() {
        return top == 0;
    }

    //Print stack method
    public void printStack() {
        //--    Printing our stack from top to bottom
        for(int index = top-1; index >= 0; index--) {
            System.out.println(stack[index]);
        }
    }
}
