package com.erlandas;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);
        Employee billEnd = new Employee("Bill", "End", 78);

        Map<String,Employee> hashMap = new HashMap<>();
        hashMap.put(janeJones.getLastName(),janeJones);
        hashMap.put(johnDoe.getLastName(),johnDoe);
        hashMap.put(marySmith.getLastName(),marySmith);
        hashMap.put(mikeWilson.getLastName(),mikeWilson);
        hashMap.put(billEnd.getLastName(),billEnd);

        //Employee employee = hashMap.put("Wilson",johnDoe);
        Employee employee = hashMap.putIfAbsent("Wilson",johnDoe);
        System.out.println(employee);

        System.out.println(hashMap.getOrDefault("someone", new Employee("None","None",0)));
        System.out.println(hashMap.remove("Jones"));

        System.out.println(hashMap.containsKey("Jones"));
        System.out.println(hashMap.containsValue(mikeWilson));



        //Iterate over first method
//        Iterator<Employee> iterator = hashMap.values().iterator();
//        while(iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }

        //Iterate over second method
       //hashMap.forEach((key,value) -> System.out.println("Key = " + key + ", Value = " + value));
    }
}
