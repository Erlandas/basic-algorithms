package com.erlandas;

public class SimpleHashtable {

    //--    We need an array that we are going to use
    //--    to back the hashtable
    private Employee[] hashtable;

    //--    Constructor that creates our array
    public SimpleHashtable() {
        hashtable = new Employee[10];
    }

    //--    Method that let's callers put stuff into a hashtable
    //--    We need (Key, value that we are going to add)
    public void put(String key, Employee employee) {

        int hashedkey = hashKey(key); // it will give us index in the array

        //-- if place in the array is not empty
        if(hashtable[hashedkey] != null) {
            System.out.println("Sorry, there's already an employee at position " + hashedkey);
        } else {
            hashtable[hashedkey] = employee;
        }
    }

    //--    Method for retrieving from hashtable
    public Employee get(String key) {
        int hashedKey = hashKey(key); // We get key from method bellow
        return hashtable[hashedKey];
    }

    //--    Hashing function
    //--    Hash value is always an integer
    private int hashKey(String key) {
        return key.length() % hashtable.length;
    }

    //--    print hashtable method
    public void printHashtable() {
        for(int i = 0; i < hashtable.length; i++) {
            System.out.println("Position #" +i+ "  :  " + hashtable[i]);
        }
    }

}
