package com.erlandas;

public class StoredEmployee {

    //LINEAR PROBING
    //--    This class will have two fields
    //--    1.  key -> this is the raw key not the hashed value
    //--    2.  Employee
    //--    for example we making public variables and methods

    public String key;
    public Employee employee;

    public StoredEmployee(String key, Employee employee) {
        this.key = key;
        this.employee = employee;
    }
}
