package com.erlandas;

public class SimpleHashtable {

    //--    We need an array that we are going to use
    //--    to back the hashtable
    //--    Linear probing
    private StoredEmployee[] hashtable;

    //--    Constructor that creates our array
    public SimpleHashtable() {
        hashtable = new StoredEmployee[10];
    }

    //--    LINEAR PROBING updated
    //--    Method that let's callers put stuff into a hashtable
    //--    We need (Key, value that we are going to add)
    public void put(String key, Employee employee) {

        int hashedKey = hashKey(key); // it will give us index in the array

        if(occupied(hashedKey)) {
            //--    First set stop index, we have to know when to stop looking
            //--    this implementation is going to wrap
            //--    if we hit the end of the array
            //--    we loop around until we find an empty spot or we hit stop index
            int stopIndex = hashedKey;
            //--    this is the wrapping, it will set first probe position
            //--    and it take cares of wrapping and that's it
            if(hashedKey == hashtable.length-1) {
                hashedKey = 0;
            } else {
                hashedKey++;
            }

            //--    on each loop iteration this loop will set next probe position
            while(occupied(hashedKey) && hashedKey != stopIndex) {
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }

        //-- if we are not going to find an empty slot if we hit this IF that means array is full
        if(occupied(hashedKey)) {
            System.out.println("Sorry, there's already an employee at position " + hashedKey);
        } else { //--   otherwise we add an employee
            hashtable[hashedKey] = new StoredEmployee(key, employee);
        }
    }

    //--    Method for retrieving from hashtable
    public Employee get(String key) {
        int hashedKey = findKey(key); // We get key from method bellow
        if(hashedKey == -1) {
            return null;
        }
        return hashtable[hashedKey].employee;
    }

    //--    Hashing function
    //--    Hash value is always an integer
    private int hashKey(String key) {
        return key.length() % hashtable.length;
    }

    private int findKey(String key) {
        int hashedKey = hashKey(key);

        if(hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key)) {
            return hashedKey;
        }

        int stopIndex = hashedKey;

        if(hashedKey == hashtable.length-1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }

        while(hashedKey != stopIndex &&
                hashtable[hashedKey] != null &&
                    !hashtable[hashedKey].key.equals(key)) {
            hashedKey = (hashedKey + 1) % hashtable.length;
        }

        if(stopIndex == hashedKey) {
            return -1;
        } else {
            return hashedKey;
        }
    }

    //--    LINEAR PROBING
    //--    method to check if a position is already occupied
    //--    we accepting index because we are going to be checking
    //--    whether a specific position is occupied
    private boolean occupied(int index) {
        //--    we are going to return if its occupied
        //--    false -> if its equal to null that means its empty position
        //--    true -> if its not equal to null
        return hashtable[index] != null;

    }


    //--    print hashtable method
    public void printHashtable() {
        for(int i = 0; i < hashtable.length; i++) {
            if(hashtable[i] ==null) {
                System.out.println("empty");
            } else {
                System.out.println("Position #" + i + "  :  " + hashtable[i].employee);
            }
        }
    }

}
