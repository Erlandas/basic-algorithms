package com.erlandas;

import java.util.LinkedList;
import java.util.ListIterator;

public class ChainedHashtable {

    //--    we need 1 field and that is our hashtable
    private LinkedList<StoredEmployee>[] hashtable;

    public ChainedHashtable(){
        hashtable = new LinkedList[10];
        //then we have to initialize each array position with linked list
        for (int i = 0; i < hashtable.length; i++) {
            hashtable[i] = new LinkedList<>();
        }
    }

    public void put(String key, Employee employee) {
        int hashedKey = hashKey(key);
        hashtable[hashedKey].add(new StoredEmployee(key,employee));
    }

    public Employee get(String key) {
        int hashedKey = hashKey(key);
        //And now we need to search the linked list at position hashedKey for the StoredEmployee
        ListIterator<StoredEmployee> iterator = hashtable[hashedKey].listIterator();
        StoredEmployee employee = null;
        while(iterator.hasNext()) {
            employee = iterator.next();
            if (employee.key.equals(key)) {
                return employee.employee;
            }
        }

        //Now if we drop out of this loop that means that we have iterated
        //over the entire list and we have not found a StoredEmployee instance
        return null;
    }

    public Employee remove(String key) {
        int hashedKey = hashKey(key);
        //And now we need to search the linked list at position hashedKey for the StoredEmployee
        ListIterator<StoredEmployee> iterator = hashtable[hashedKey].listIterator();
        StoredEmployee employee = null;
        int index = -1;
        while(iterator.hasNext()) {
            employee = iterator.next();
            index++;
            if (employee.key.equals(key)) {
                break;
            }
        }

        if(employee == null) {
            //Now if we drop out of this loop that means that we have iterated
            //over the entire list and we have not found a StoredEmployee instance
            return null;
        } else {
            hashtable[hashedKey].remove(index);
            return employee.employee;
        }
    }

    private int hashKey(String key) {
        //return key.length() % hashtable.length;
        return Math.abs(key.hashCode() % hashtable.length);
    }

    public void printHashtable() {
        for(int i = 0; i < hashtable.length; i++) {
            if (hashtable[i].isEmpty()) {
                System.out.println("Position #" +i+ " : empty");
            } else {
                System.out.print("Position #" +i+ " : ");
                ListIterator<StoredEmployee> iterator = hashtable[i].listIterator();
                while(iterator.hasNext()) {
                    System.out.print(iterator.next().employee);
                    System.out.print("->");
                }
                System.out.println("null");
            }
        }
    }
}
