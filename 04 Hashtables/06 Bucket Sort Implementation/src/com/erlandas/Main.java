package com.erlandas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        int[] array = {54,46,83,66,95,92,43};

        bucketSort(array);

        for(int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

    public static void bucketSort(int[] input) {
        //1.    Create our bucket
        List<Integer>[] buckets = new List[10];

        //2.    Create an array list at each bucket
        for(int i = 0; i < buckets.length; i++) {
            buckets[i] = new ArrayList<>();
        }

        //3.    Traverse an array and put each value in appropriate bucket
        for(int i = 0; i < input.length; i++) {
            buckets[hash(input[i])].add(input[i]);
        }

        //4.    Sort each bucket
        for (List bucket: buckets) {
            Collections.sort(bucket);
        }

        //5.    Gathering stage
        //--    outer loop is traversing buckets array
        //--    inner loop is traversing the array list that's at buckets[i]
        //--    So we are saying for each bucket traverse that bucket and copy the values into array
        int j = 0;
        for (int i = 0; i < buckets.length; i++) {
            for (int value: buckets[i]) {
                input[j++] = value;
            }
        }


    }

    //Hashing function
    private static int hash(int value) {
        //returns first number : 54/10 = 5, 66/10 = 6
        return value / (int) 10;
    }
}
