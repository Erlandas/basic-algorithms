package com.erlandas;

import java.util.*;

/**
 * 1.   Remove duplicate items from a linked list
 * 2.   Your solution must use the JDK's LinkedList class
 * 3.   Your solution must use a hashMap
 * 4.   Use the starter project in the resources section
 */

public class Main {

    public static void main(String[] args) {

        LinkedList<Employee> employees = new LinkedList<>();
        employees.add(new Employee("Jane", "Jones", 123));
        employees.add(new Employee("John", "Doe", 5678));
        employees.add(new Employee("Mike", "Wilson", 45));
        employees.add(new Employee("Mary", "Smith", 5555));
        employees.add(new Employee("John", "Doe", 5678));
        employees.add(new Employee("Bill", "End", 3948));
        employees.add(new Employee("Jane", "Jones", 123));

        employees.forEach(e -> System.out.println(e));

        //1.    Create hash map
        HashMap<Integer,Employee> employeeTable = new HashMap<>();
        ListIterator<Employee> iterator = employees.listIterator();
        //2.    List to save duplicates
        List<Employee> remove = new ArrayList<>();

        while(iterator.hasNext()) {
            Employee employee = iterator.next();
            if(employeeTable.containsKey(employee.getId())) {
                remove.add(employee);
            } else {
                employeeTable.put(employee.getId(),employee);
            }
        }

        //remove duplicates
        for(Employee employee: remove) {
            employees.remove(employee);
        }

        System.out.println("--------------------------------------------------");
        employees.forEach(e -> System.out.println(e));
    }
}
