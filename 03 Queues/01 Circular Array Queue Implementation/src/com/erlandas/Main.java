package com.erlandas;

/**
 * In this implementation we will be improving the code by updating
 * let say, we have two item in our queue and then we remove one add one and so on
 * in this case we always will have max 2 items in a queue and by doing so
 * we will be incrementing front variable and we will have to resize array even if we have only 2 items
 * in the array
 */
public class Main {

    public static void main(String[] args) {

        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);
        Employee billEnd = new Employee("Bill", "End", 78);

        ArrayQueue queue = new ArrayQueue(10);
        queue.add(janeJones);
        queue.add(johnDoe);
        queue.add(marySmith);
        queue.add(mikeWilson);
        queue.add(billEnd);

        queue.printQueue();

        System.out.println("Popped -> " + queue.remove());
        System.out.println();
        queue.printQueue();
        System.out.println();
        System.out.println("Peek -> " +queue.peek());
        System.out.println();
        queue.printQueue();
    }
}
