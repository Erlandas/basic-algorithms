package com.erlandas;

import java.util.NoSuchElementException;

public class ArrayQueue {
    //We need three fields for this queue
    //--    1.  For the array
    //--    2.  To track the front of the queue
    //--    3.  To track back of the queue
    private Employee[] queue;
    private int front;
    private int back;

    //When we start front and back variables will be 0 so we don't need to initialise those
    //in the constructor, we just need to create out array
    public ArrayQueue(int capacity) {
        queue = new Employee[capacity];
    }

    //First adding elements method, we want to add elements to the back of the queue.
    //And we will have to worry about resizing the array when the queue is full.
    //So for this implementation the front element is always at the index front
    //and the back element is always at index back minus one
    //So back field always will be pointing to the next available position in the queue.
    //If the back is, let's say, pointing to position three then that means
    //that the end of the queue is actually at position two.
    //Back value is where we add the next element that's coming into the queue.
    //When we add item we increment back by one, and front field never changes because
    //we always add items to the back.
    public void add(Employee employee) {
        //CIRCULAR QUEUE -> if we push front to top and there is empty spaces in the array we will be using
        //                  those empty spaces to store our last element, this prevents unneeded resizing
        //                  and also it will use empty spaces in the array
        //--    first we check if size of the array is equals length of the array
        //--    this way we find out if we have empty spaces in the array to store more elements
        if(size() == queue.length-1) {
            //If we get to a situation that front value can cross back value
            //we have to resize the array
            //Code will be improved that when we resize the array we will be putting them in correct order
            //from front to back, if we wont resize the array back variable will be
            //overwriting front and we will be loosing items
            int numItems = size()
            Employee[] newArray = new Employee[2 * queue.length];
            //--    Copy first added items to the front of the array
            //--                                            how many items: eg length 5 front 3 = 5-3 = 2
            //--                                            two items to copy over
            System.arraycopy(queue,front,newArray,0,queue.length-front);
            //--    Copy back items followed by front items
            //--                                    where to start copy 5-3 = 2
            //--                                    hav many items back = 3
            //--    So we start copying back items and thats how we reset and resize our array in order
            System.arraycopy(queue,0,newArray,queue.length-front,back);
            queue = newArray;

            //Set new front and new back
            front = 0;
            back = numItems;


        }



        queue[back] = employee;
        if(back < queue.length-1) {
            back++;
        } else {
            back = 0;
        }

    }

    //Method to remove element from the queue
    //we always remove first element in the queue
    public Employee remove() {
        //First thing we have to check if the queue is empty
        if(size() == 0) {
            throw new NoSuchElementException();
        }

        Employee employee = queue[front];
        queue[front] = null; //Not important just clean up
        front++; //set new front of the queue

        //Optimization we can do at this point
        //if we removed only item in the queue
        //then we can reset front and back positions to 0
        if(size() == 0){
            front = 0;
            back = 0;
        }
        //--    If that's true that means following item added in the queue is at position 0
        else if (front == queue.length) {
            front = 0;
        }
        return employee;

    }

    //Peek method implementation returns first item in the queue
    public Employee peek(){
        //First thing we have to check if the queue is empty
        if(size() == 0) {
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public int size() {
        //we testing if the queue was wrapped
        //--    if front is less or equal to back that means queue wasnt wrapped
        if(front <= back) {
            return back - front;
        } else {
            return back - front + queue.length;
        }

    }

    public void printQueue() {
        //We have to keep in mind that front can be more than 0
        if(front <= back) {//That means it wasn't wrapped
            for(int i = front; i < back;i++){
                System.out.println(queue[i]);
            }
        } else {
            for (int i = front; i < queue.length; i++) {
                System.out.println(queue[i]);
            }
            for (int i = 0; i < back; i++) {
                System.out.println(queue[i]);
            }
        }

    }

}
