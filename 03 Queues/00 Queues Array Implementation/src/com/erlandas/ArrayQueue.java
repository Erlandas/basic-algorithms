package com.erlandas;

import java.util.NoSuchElementException;

public class ArrayQueue {
    //We need three fields for this queue
    //--    1.  For the array
    //--    2.  To track the front of the queue
    //--    3.  To track back of the queue
    private Employee[] queue;
    private int front;
    private int back;

    //When we start front and back variables will be 0 so we don't need to initialise those
    //in the constructor, we just need to create out array
    public ArrayQueue(int capacity) {
        queue = new Employee[capacity];
    }

    //First adding elements method, we want to add elements to the back of the queue.
    //And we will have to worry about resizing the array when the queue is full.
    //So for this implementation the front element is always at the index front
    //and the back element is always at index back minus one
    //So back field always will be pointing to the next available position in the queue.
    //If the back is, let's say, pointing to position three then that means
    //that the end of the queue is actually at position two.
    //Back value is where we add the next element that's coming into the queue.
    //When we add item we increment back by one, and front field never changes because
    //we always add items to the back.
    public void add(Employee employee) {
        //First thing we are going to do is check whether the queue is full
        //The queue will be full if back equals the length of the queue.
        if(back == queue.length) {
            Employee[] newArray = new Employee[2 * queue.length];
            System.arraycopy(queue,0,newArray,0,queue.length);
            queue = newArray;
        }

        queue[back] = employee;
        back++;
    }

    //Method to remove element from the queue
    //we always remove first element in the queue
    public Employee remove() {
        //First thing we have to check if the queue is empty
        if(size() == 0) {
            throw new NoSuchElementException();
        }

        Employee employee = queue[front];
        queue[front] = null; //Not important just clean up
        front++; //set new front of the queue

        //Optimization we can do at this point
        //if we removed only item in the queue
        //then we can reset front and back positions to 0
        if(size() == 0){
            front = 0;
            back = 0;
        }
        return employee;

    }

    //Peek method implementation returns first item in the queue
    public Employee peek(){
        //First thing we have to check if the queue is empty
        if(size() == 0) {
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public int size() {
        return back - front;
    }

    public void printQueue() {
        //We have to keep in mind that front can be more than 0
        for(int i = front; i < back;i++){
            System.out.println(queue[i]);
        }
    }

}
